import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from '../../components/home/home.component';
import {NotFoundComponent} from '../../components/not-found/not-found.component';
import { UserPageComponent } from 'src/app/components/user-page/user-page.component';
import { ReferralComponent } from 'src/app/components/referral/referral.component';
import { HistoryVisitComponent } from 'src/app/components/history-visit/history-visit.component';
import { ActiveQueuePlaceComponent } from 'src/app/components/active-queue-place/active-queue-place.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: '404', component: NotFoundComponent},
  { path: 'user', component: UserPageComponent},
  { path: 'referral', component: ReferralComponent},
  { path: 'history-visit', component: HistoryVisitComponent},
  { path: 'active-queue-place', component: ActiveQueuePlaceComponent},
  { path: '**', redirectTo: '/404'}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ], exports: [
    RouterModule
  ]
})
export class RoutingModule { }
