import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DoctorSpeciality} from '../../models/entity/doctor-speciality';
import {ApiUrls} from '../../const/api-urls';

@Injectable({
  providedIn: 'root'
})
export class SpecialitySearchService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  public getDoctorSpecialityByName(name: string): Observable<DoctorSpeciality> {
    return this.httpClient.get<DoctorSpeciality>(ApiUrls.SPECIALITY_GET_BY_NAME + name);
  }

  public getDoctorSpecialitiesInHospital(hospitalId: number): Observable<string[]> {
    return this.httpClient.get<string[]>(ApiUrls.SPECIALITY_GET_BY_HOSPITAL + hospitalId);
  }
}
