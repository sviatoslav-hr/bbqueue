import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ReferralDto } from 'src/app/models/dto/referral-dto';
import {ApiUrls} from 'src/app/const/api-urls';


@Injectable({
  providedIn: 'root'
})
export class ReferralService {

  constructor(
    private http: HttpClient
  ) {
  }

public getReferralForUser(): Observable<ReferralDto[]> {
  return this.http.get<ReferralDto[]>(ApiUrls.REFERRAL_GET_FOR_USER);
  }
}
