import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DoctorSearchFormDto} from '../../models/dto/doctor-search-form-dto';
import {ApiUrls} from '../../const/api-urls';

@Injectable({
  providedIn: 'root'
})
export class DoctorSearchService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  public getDoctorsByName(hospitalId: number, lastName: string): Observable<DoctorSearchFormDto[]> {
    const params = new HttpParams()
      .set('hospitalId', String(hospitalId))
      .set('lastName', lastName);
    return this.httpClient.get<DoctorSearchFormDto[]>(ApiUrls.DOCTORS_GET_BY_NAME, {params});
  }

  public getDoctorsBySpeciality(specialityName: string, hospitalId: number): Observable<DoctorSearchFormDto[]> {
    const params = new HttpParams()
      .set('specialityName', specialityName)
      .set('hospitalId', String(hospitalId));
    return this.httpClient.get<DoctorSearchFormDto[]>(ApiUrls.DOCTORS_GET_BY_SPECIALITY, {params});
  }

  public getDoctorsBySpecialityAndLastName(specialityId: number, hospitalId: number, lastName: string): Observable<DoctorSearchFormDto[]> {
    const params = new HttpParams()
      .set('specialityId', String(specialityId))
      .set('hospitalId', String(hospitalId))
      .set('lastName', lastName);
    return this.httpClient.get<DoctorSearchFormDto[]>(ApiUrls.DOCTORS_GET_BY_SPECIALITY_LAST_NAME, {params});
  }

  public getDoctorsByHospital(hospitalId: number): Observable<DoctorSearchFormDto[]> {
    return this.httpClient.get<DoctorSearchFormDto[]>(ApiUrls.DOCTORS_GET_BY_HOSPITAL_ID + hospitalId);
  }
}
