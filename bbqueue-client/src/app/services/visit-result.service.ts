import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TimeSlotDto } from '../models/dto/time-slot-dto';
import { ApiUrls } from 'src/app/const/api-urls';

@Injectable({
  providedIn: 'root'
})
export class VisitResultService {

  constructor(
    private http: HttpClient
  ) { }

  public getAllHistoryOfVisitsForUser(): Observable<TimeSlotDto[]> {
    return this.http.get<TimeSlotDto[]>(ApiUrls.HISTORY_GET_ALL_VISITS_FOR_USER);
  }

  public getAllActiveQueuePlace(): Observable<TimeSlotDto[]> {
    return this.http.get<TimeSlotDto[]>(ApiUrls.ACTIVE_GET_ALL_QUEUE_PLACE);
  }
}
