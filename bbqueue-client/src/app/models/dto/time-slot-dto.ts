import { User } from '../entity/user';
import { DoctorService } from '../entity/doctor-service';
import { VisitResult } from '../entity/visit-result';

export class TimeSlotDto {
  constructor(
    public id: number,
    public visitDate: number,
    public timeSlotNumber: number,
    public customer: User,
    public service: DoctorService,
    public visitResult: VisitResult
  ) {}
}
