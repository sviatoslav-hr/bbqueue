export class ReferralDto {
  constructor(
    public id: number,
    public description: string,
    public doctorFromName: string,
    public doctorToName: string,
    public expirationDate: number,
    public customerName: string
  ) {
  }
}
