import { User } from './user';
import { DoctorService } from './doctor-service';
import { VisitResult } from './visit-result';

export class TimeSlot {
    constructor(
        public id: number,
        public visitDate: Date,
        public timeSlotNumber: number,
        public customer: User,
        public service: DoctorService,
        public visitResult: VisitResult
    ) {}
}
