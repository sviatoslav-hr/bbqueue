export class VisitResult {
    constructor(
        public id: number,
        public description: string
    ){}
}
