import { Doctor } from './doctor';

export class DoctorService {
  constructor(
    public id: number,
    public doctor: Doctor,
    public name: string,
    public timeSlotAmount: number
  ) {}
}
