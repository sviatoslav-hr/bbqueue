export class Referral {
  constructor(
    public id: number,
    public description: string,
    public doctorFromName: string,
    public doctorToName: string,
    public expirationDate: Date,
    public customerName: string
  ) {
  }
}
