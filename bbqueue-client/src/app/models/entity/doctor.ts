import {User} from './user';
import {Hospital} from './hospital';
import {DoctorSpeciality} from './doctor-speciality';
import {DoctorWorkDay} from './doctor-work-day';
import {DoctorService} from './doctor-service';

export class Doctor {
  constructor(
    public id: number,
    public cabinetNumber: number,
    public user: User,
    public hospital: Hospital,
    public speciality: DoctorSpeciality,
    public doctorWorkDays: DoctorWorkDay[],
    public services: DoctorService[],
    public customers: User
  ) {}

}
