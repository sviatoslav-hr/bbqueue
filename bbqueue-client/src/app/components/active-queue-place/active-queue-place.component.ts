import { Component, OnInit, Input } from '@angular/core';
import { TimeSlot } from 'src/app/models/entity/time-slot';
import { VisitResultService } from 'src/app/services/visit-result.service';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { TimeSlotDto } from 'src/app/models/dto/time-slot-dto';

@Component({
  selector: 'app-active-queue-place',
  templateUrl: './active-queue-place.component.html',
  styleUrls: ['./active-queue-place.component.css']
})
export class ActiveQueuePlaceComponent implements OnInit {

  timeSlots: TimeSlot[] = [];
  timeSlotsDto: TimeSlotDto[];

  constructor(
    private visitResultService: VisitResultService,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this.visitResultService.getAllActiveQueuePlace().subscribe(data => {
      this.timeSlotsDto = data;
      for (const value of this.timeSlotsDto){
        this.timeSlots.push(new TimeSlot(value.id, new Date(value.visitDate),
          value.timeSlotNumber, value.customer, value.service, value.visitResult ));
      }
      this.timeSlots.sort((val1, val2) => val1.visitDate > val2.visitDate ? 1 : -1);
    });
  }

}
