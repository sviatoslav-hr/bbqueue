import {Component, OnInit} from '@angular/core';
import {RegionSearchService} from '../../../services/region/region-search.service';
import {Region} from '../../../models/entity/region';

@Component({
  selector: 'app-region-search',
  templateUrl: './region-search.component.html',
  styleUrls: ['./region-search.component.css']
})
export class RegionSearchComponent implements OnInit {
  private region: Region;
  private regionName: string;
  private regionNames: string[];
  private savedNames: string[];

  constructor(
    private searchRegionService: RegionSearchService,
  ) {
  }

  ngOnInit() {
    this.getRegionNames();
  }

  private getRegionNames() {
    this.searchRegionService.getAllRegionNames().subscribe(data => {
      this.savedNames = data;
      this.regionNames = data;
    });
  }

  onClickToGetRegion(regionName) {
    if (this.savedNames.map(value => value.toLowerCase()).includes(this.regionName.toLowerCase() || regionName)) {
      this.searchRegionService.getRegionByName(regionName).subscribe(region => {
        this.region = region;
      });
    }
  }

  clickBackSpace() {
    this.region = null;
  }

  inputToGetRegion() {
    this.regionNames = this.savedNames.filter(value => value.toLowerCase().includes(this.regionName.toLowerCase()));
    if (this.regionNames !== undefined) {
      this.region = null;
    }
  }

  clickToSelectRegion(e) {
    if (this.regionName !== undefined) {
      this.onClickToGetRegion(e.target.value);
    }
  }
}
