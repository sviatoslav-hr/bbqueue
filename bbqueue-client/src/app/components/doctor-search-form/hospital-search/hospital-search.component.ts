import {Component, Input, OnInit} from '@angular/core';
import {HospitalSearchService} from '../../../services/hospital/hospital-search.service';
import {Hospital} from '../../../models/entity/hospital';
import {Region} from '../../../models/entity/region';

@Component({
  selector: 'app-hospital-search',
  templateUrl: './hospital-search.component.html',
  styleUrls: ['./hospital-search.component.css']
})
export class HospitalSearchComponent implements OnInit {
  @Input() region: Region;
  private hospital: Hospital;
  private hospitalName: string;
  private hospitalNames: string[];
  private savedNames: string[];

  constructor(
    private hospitalSearchService: HospitalSearchService,
  ) {
  }

  ngOnInit() {
    this.getHospitalNames();
  }

  private getHospitalNames() {
    this.hospitalSearchService.getAllHospitalNames(this.region.id).subscribe(data => {
      this.savedNames = data;
      this.hospitalNames = data;
    });
  }

  onClickToGetHospital(hospitalName) {
    if (this.savedNames.map(value => value.toLowerCase()).includes(this.hospitalName.toLowerCase())) {
      this.hospitalSearchService.getHospitalByNameAndRegion(hospitalName, this.region.id).subscribe(hospital => {
        this.hospital = hospital;
      });
    }
  }

  clickBackSpace() {
    this.hospital = null;
  }

  inputToGetHospital() {
    this.hospitalNames = this.savedNames.filter(value => value.toLowerCase().includes(this.hospitalName.toLowerCase()));
    if (this.hospitalNames !== undefined) {
      this.hospital = null;
    }
  }

  clickToSelectHospital(e) {
    this.onClickToGetHospital(e.target.value);
  }
}
