import {Component, Input, OnInit} from '@angular/core';
import {Hospital} from '../../../models/entity/hospital';
import {DoctorSpeciality} from '../../../models/entity/doctor-speciality';
import {SpecialitySearchService} from '../../../services/speciality/speciality-search.service';

@Component({
  selector: 'app-speciality-search',
  templateUrl: './speciality-search.component.html',
  styleUrls: ['./speciality-search.component.css']
})
export class SpecialitySearchComponent implements OnInit {

  @Input() hospital: Hospital;
  private savedSpecialitiesInHospital: string[];
  private specialitiesInHospital: string[];
  private specialityName: string;
  private doctorSpeciality: DoctorSpeciality;

  constructor(
    private specialitySearchService: SpecialitySearchService,
  ) {
  }

  ngOnInit() {
    this.specialitySearchService.getDoctorSpecialitiesInHospital(this.hospital.id)
      .subscribe(specialities => {
        this.specialitiesInHospital = specialities;
        this.savedSpecialitiesInHospital = specialities;
      });
  }

  onClickToGetSpeciality(specialityName) {
    if (this.savedSpecialitiesInHospital.map(value => value.toLowerCase()).includes(this.specialityName.toLowerCase())) {
      this.specialitySearchService.getDoctorSpecialityByName(specialityName).subscribe(speciality => {
        this.doctorSpeciality = speciality;
      });
    }
  }

  inputToGetSpeciality() {
    this.specialitiesInHospital = this.savedSpecialitiesInHospital
      .filter(value => value.toLowerCase().includes(this.specialityName.toLowerCase()));
    if (this.specialitiesInHospital !== undefined) {
      this.doctorSpeciality = null;
    }
  }

  clickToSelectSpeciality(e) {
    this.onClickToGetSpeciality(e.target.value);
  }

  clickBackSpace() {
    this.doctorSpeciality = null;
  }
}
