import {Component, Input, OnInit} from '@angular/core';
import {Hospital} from '../../../models/entity/hospital';
import {DoctorSearchService} from '../../../services/doctor/doctor-search.service';
import {DoctorSearchFormDto} from '../../../models/dto/doctor-search-form-dto';
import {DoctorSpeciality} from '../../../models/entity/doctor-speciality';

@Component({
  selector: 'app-doctor-search',
  templateUrl: './doctor-search.component.html',
  styleUrls: ['./doctor-search.component.css']
})
export class DoctorSearchComponent implements OnInit {

  @Input() hospital: Hospital;
  @Input() doctorSpeciality: DoctorSpeciality;
  private doctorsInHospital: DoctorSearchFormDto[];
  private doctorsWithSpeciality: DoctorSearchFormDto[];
  private doctorsWithDefinedNameAndSpeciality: DoctorSearchFormDto[];
  private doctorsWithDefinedName: DoctorSearchFormDto[];
  private doctorLastNamesWithSpeciality: string[];
  private savedDoctorLastNamesWithSpeciality: string[];
  private doctorLastNames: string[];
  private savedDoctorLastNames: string[];
  private doctorsWithNameWithSpeciality: DoctorSearchFormDto[];
  private doctorName: string;
  private doctorNameWithSpeciality: string;

  constructor(
    private doctorSearchService: DoctorSearchService,
  ) {
  }

  ngOnInit() {
    this.getDoctorsInHospital();
    this.getDoctorsWithSpeciality();
  }

  getDoctorsInHospital() {
    this.doctorSearchService.getDoctorsByHospital(this.hospital.id).subscribe(doctorsInHospital => {
      this.doctorsInHospital = doctorsInHospital;
      this.doctorLastNames = this.getDistinctName(doctorsInHospital);
      this.savedDoctorLastNames = this.getDistinctName(doctorsInHospital);
    });
  }

  getDoctorsWithSpeciality() {
    if (this.doctorSpeciality !== undefined) {
      this.doctorSearchService.getDoctorsBySpeciality(this.doctorSpeciality.name, this.hospital.id)
        .subscribe(doctorsWithSpeciality => {
          this.doctorsWithSpeciality = doctorsWithSpeciality;
          this.doctorLastNamesWithSpeciality = this.getDistinctName(doctorsWithSpeciality);
          this.savedDoctorLastNamesWithSpeciality = this.getDistinctName(doctorsWithSpeciality);
        });
    }
  }

  getDistinctName(doctorsWithSpeciality): string[] {
    return doctorsWithSpeciality.map(value => value.lastName)
      .filter((value, index, self) => self.indexOf(value) === index);
  }

  onClickToGetDoctorName(doctorName) {
    if (this.savedDoctorLastNames.map(value => value.toLowerCase()).includes(this.doctorName.toLowerCase())) {
      this.doctorSearchService.getDoctorsByName(this.hospital.id, doctorName).subscribe(doctors => {
        this.doctorsWithDefinedName = doctors;
      });
    }
  }

  inputToGetDoctorsWithName() {
    this.doctorLastNames = this.savedDoctorLastNames.filter(value => value.toLowerCase()
      .includes(this.doctorName.toLowerCase()));
    if (this.doctorLastNames !== undefined) {
      this.doctorsWithDefinedName = undefined;
    }
  }

  clickToSelectDoctorsByName(e) {
    if (this.doctorSpeciality !== undefined) {
      this.onClickToGetDoctorNameWithSpeciality(e.target.value);
    } else {
      this.onClickToGetDoctorName(e.target.value);
    }
  }

  clickBackSpaceForDoctorNames() {
    if (this.doctorSpeciality !== undefined) {
      this.doctorsWithDefinedNameAndSpeciality = undefined;
    } else {
      this.doctorsWithDefinedName = undefined;
    }
  }

  onClickToGetDoctorNameWithSpeciality(doctorName) {
    if (this.savedDoctorLastNamesWithSpeciality.map(value => value.toLowerCase())
      .includes(this.doctorNameWithSpeciality.toLowerCase())) {
      this.doctorSearchService
        .getDoctorsBySpecialityAndLastName(this.doctorSpeciality.id, this.hospital.id, doctorName)
        .subscribe(doctors => {
          this.doctorsWithDefinedNameAndSpeciality = doctors;
        });
    }
  }

  inputToGetDoctorsWithNameAndSpeciality() {
    this.doctorLastNamesWithSpeciality = this.savedDoctorLastNamesWithSpeciality
      .filter(value => value.toLowerCase().includes(this.doctorNameWithSpeciality.toLowerCase()));
    if (this.doctorLastNamesWithSpeciality !== undefined) {
      this.doctorsWithDefinedNameAndSpeciality = undefined;
    }
  }

  checkingDoctorsForOutput() {
    if (this.doctorsWithDefinedNameAndSpeciality !== undefined && this.doctorSpeciality !== undefined) {
      return this.doctorsWithDefinedNameAndSpeciality;
    } else if (this.doctorSpeciality !== undefined) {
      return this.doctorsWithSpeciality;
    } else if (this.doctorsWithDefinedName !== undefined) {
      return this.doctorsWithDefinedName;
    } else {
      return this.doctorsInHospital;
    }
  }
}
