import { Component, OnInit, Input } from '@angular/core';
import { TimeSlot } from 'src/app/models/entity/time-slot';
import { VisitResultService } from 'src/app/services/visit-result.service';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { TimeSlotDto } from 'src/app/models/dto/time-slot-dto';

@Component({
  selector: 'app-history-visit',
  templateUrl: './history-visit.component.html',
  styleUrls: ['./history-visit.component.css']
})
export class HistoryVisitComponent implements OnInit {

  timeSlots: TimeSlot[] = [];
  timeSlotsDto: TimeSlotDto[];

  constructor(
    private visitResultService: VisitResultService,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this.visitResultService.getAllHistoryOfVisitsForUser().subscribe(data => {
      this.timeSlotsDto = data;
      for (const value of this.timeSlotsDto) {
        this.timeSlots.push(new TimeSlot(value.id, new Date(value.visitDate),
          value.timeSlotNumber, value.customer, value.service, value.visitResult));
      }
      this.timeSlots.sort((val1, val2) => val1.visitDate > val2.visitDate ? 1 : -1);
    });
  }
}
