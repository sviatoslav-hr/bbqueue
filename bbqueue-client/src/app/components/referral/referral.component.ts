import {Component, OnInit, Input} from '@angular/core';
import {Referral} from 'src/app/models/entity/referral';
import {ReferralService} from 'src/app/services/referral.service';
import {AuthenticationService} from 'src/app/services/authentication/authentication.service';
import {ReferralDto} from '../../models/dto/referral-dto';

@Component({
  selector: 'app-referral',
  templateUrl: './referral.component.html',
  styleUrls: ['./referral.component.css']
})
export class ReferralComponent implements OnInit {

  referrals: Referral[] = [];
  referralsDto: ReferralDto[];

  constructor(
    private referralService: ReferralService,
    private authenticationService: AuthenticationService
  ) {
  }

  ngOnInit() {
    this.referralService.getReferralForUser().subscribe(data => {
      this.referralsDto = data;
      for (const value of this.referralsDto) {
        this.referrals.push(new Referral(value.id, value.description, value.doctorFromName,
          value.doctorToName, new Date(value.expirationDate), value.customerName));
      }
      this.referrals.sort((val1, val2) => val1.expirationDate > val2.expirationDate ? 1 : -1);
    });
  }
}
