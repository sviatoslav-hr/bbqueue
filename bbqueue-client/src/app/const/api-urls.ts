import {environment} from '../../environments/environment';

export class ApiUrls {
  public static SPECIALITY_GET_BY_HOSPITAL = `${environment.apiUrl}/speciality/in-hospital/`;
  public static SPECIALITY_GET_BY_NAME = `${environment.apiUrl}/speciality/`;
  public static HOSPITAL_GET_ALL_NAMES = `${environment.apiUrl}/hospital/all/`;
  public static REGION_GET_ALL_NAMES = `${environment.apiUrl}/region/all`;
  public static DOCTORS_GET_BY_SPECIALITY_LAST_NAME = `${environment.apiUrl}/doctors/with-speciality-and-name/`;
  public static DOCTORS_GET_BY_SPECIALITY = `${environment.apiUrl}/doctors/with-speciality/`;
  public static DOCTORS_GET_BY_NAME = `${environment.apiUrl}/doctors/by-name/`;
  public static DOCTORS_GET_BY_HOSPITAL_ID = `${environment.apiUrl}/doctors/in-hospital/`;
  public static REGION_GET_BY_NAME = `${environment.apiUrl}/region/`;
  public static HOSPITAL_GET_BY_NAME_AND_REGION = `${environment.apiUrl}/hospital/`;
  public static REFERRAL_GET_FOR_USER = `${environment.apiUrl}/user/referral/`;
  public static SIGN_IN = `${environment.apiUrl}/authentication/sign-in`;
  public static SIGN_UP = `${environment.apiUrl}/authentication/sign-up`;
  public static SEND_VERIFICATION_CODE = `${environment.apiUrl}/authentication/send-verification-code`;
  public static CONFIRM_EMAIL = `${environment.apiUrl}/authentication/confirm-email`;
  public static CHANGE_PASSWORD = `${environment.apiUrl}/authentication/change-password`;
  public static PRINCIPAL_GET_BY_ID = `${environment.apiUrl}/principal/`;
  public static PRINCIPAL_GET_AUTHENTICATED = `${environment.apiUrl}/principal-authenticated`;
  public static HISTORY_GET_ALL_VISITS_FOR_USER = `${environment.apiUrl}/user/visits-history/`;
  public static ACTIVE_GET_ALL_QUEUE_PLACE = `${environment.apiUrl}/user/visits-active/`;
  public static CALENDAR_GET_BY_DOCTOR_ID = `${environment.apiUrl}/calendar/`;
}
