import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HomeComponent} from './components/home/home.component';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {httpInterceptorProviders} from './services/authentication/authentication-interceptor';
import {RoutingModule} from './modules/routing/routing.module';
import { AuthenticationComponent } from './components/authentication/authentication/authentication.component';
import { SignInComponent } from './components/authentication/sign-in/sign-in.component';
import { SignUpComponent } from './components/authentication/sign-up/sign-up.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ReferralComponent } from './components/referral/referral.component';
import { RegionSearchComponent } from './components/doctor-search-form/region-search/region-search.component';
import { VerificationComponent } from './components/authentication/verification/verification.component';
import { HistoryVisitComponent } from './components/history-visit/history-visit.component';
import { ActiveQueuePlaceComponent } from './components/active-queue-place/active-queue-place.component';
import {CalendarService} from './services/calendar/calendar.service';
import { UserPageComponent } from './components/user-page/user-page.component';
import { DoctorListComponent } from './components/doctor-list/doctor-list.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule, MatInputModule} from '@angular/material';
import { PasswordRecoveryComponent } from './components/authentication/password-recovery/password-recovery.component';
import { DoctorSearchComponent } from './components/doctor-search-form/doctor-search/doctor-search.component';
import {SpecialitySearchComponent} from './components/doctor-search-form/speciality-search/speciality-search.component';
import {HospitalSearchComponent} from './components/doctor-search-form/hospital-search/hospital-search.component';
import {DatepickerModule} from './modules/datepicker/datepicker.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    AuthenticationComponent,
    SignInComponent,
    SignUpComponent,
    NotFoundComponent,
    ReferralComponent,
    RegionSearchComponent,
    HospitalSearchComponent,
    VerificationComponent,
    PasswordRecoveryComponent,
    HistoryVisitComponent,
    UserPageComponent,
    ActiveQueuePlaceComponent,
    DoctorListComponent,
    DoctorSearchComponent,
    SpecialitySearchComponent,
    SpecialitySearchComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    DatepickerModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [
    httpInterceptorProviders,
    CalendarService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
