package com.epam.bbqueue.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class TimeSlotDto {

    @JsonSerialize(using = DateSerializer.class)
    @NotNull(message = "Date should not be null")
    private Date visitDate;

    @NotNull(message = "TimeSlot number should not be null")
    private Integer timeSlotNumber;

    @NotNull(message = "User should not be null")
    private Long customerId;

    @NotNull(message = "Service should not be null")
    private Long serviceId;
}
