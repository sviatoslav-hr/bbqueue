package com.epam.bbqueue.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Builder
public class ReferralDto {
    @NotNull
    private Long id;

    @NotNull
    private String description;

    @NotNull
    private String doctorFromName;

    @NotNull
    private String doctorToName;

    @NotNull
    private String customerName;

    @NotNull
    private Date expirationDate;
}
