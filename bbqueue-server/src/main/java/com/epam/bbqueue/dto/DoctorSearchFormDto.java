package com.epam.bbqueue.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@Builder
public class DoctorSearchFormDto {

    @NotNull
    private Long id;

    @NotNull
    private Integer cabinetNumber;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private String speciality;

    @NotNull
    private String hospital;
}
