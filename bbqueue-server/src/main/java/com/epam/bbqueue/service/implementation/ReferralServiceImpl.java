package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.aop.annotation.Loggable;
import com.epam.bbqueue.entity.Referral;
import com.epam.bbqueue.exception.ReferralNotFoundException;
import com.epam.bbqueue.repository.ReferralRepository;
import com.epam.bbqueue.service.interfaces.ReferralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReferralServiceImpl implements ReferralService {

    private final ReferralRepository referralRepository;

    @Autowired
    public ReferralServiceImpl(ReferralRepository referralRepository) {
        this.referralRepository = referralRepository;
    }

    @Loggable
    @Override
    public List<Referral> getAllReferralOfUser(Long id) {
        List<Referral> referralOfUser = referralRepository.findAllByCustomer_Id(id);
        if (referralOfUser.isEmpty()){
            throw new ReferralNotFoundException("This user has no referral (userId = " + id + ")!");
        } else {
            return referralOfUser;
        }
    }
}
