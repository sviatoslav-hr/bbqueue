package com.epam.bbqueue.service.interfaces;

import com.epam.bbqueue.entity.DoctorSpeciality;

import java.util.List;

public interface DoctorSpecialityService {

    DoctorSpeciality getDoctorSpecialityByName(String specialityName);

    List<String> getDoctorSpecialitiesByHospital(Long hospitalId);
}
