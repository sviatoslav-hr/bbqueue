package com.epam.bbqueue.service.interfaces;

import com.epam.bbqueue.entity.TimeSlot;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface VisitResultService {

    List<TimeSlot> getAllHistoryOfVisitsForUser(Long idUser, Date date);

    List<TimeSlot> getAllActiveQueuePlace(Long idUser, Date date);

}
