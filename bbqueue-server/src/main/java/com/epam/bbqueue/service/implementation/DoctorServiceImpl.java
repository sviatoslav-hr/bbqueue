package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.dto.DoctorSearchFormDto;
import com.epam.bbqueue.entity.Doctor;
import com.epam.bbqueue.exception.DoctorNotFoundException;
import com.epam.bbqueue.repository.DoctorRepository;
import com.epam.bbqueue.service.interfaces.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DoctorServiceImpl implements DoctorService {

    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorServiceImpl(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    @Override
    public List<Doctor> getDoctorsByHospitalId(Long hospitalId) {
        List<Doctor> doctorsInHospital = doctorRepository.findAllByHospital_Id(hospitalId);
        if (doctorsInHospital.isEmpty()) {
            throw new DoctorNotFoundException("In this hospital not found doctors!");
        }
        return doctorsInHospital;
    }

    @Override
    public List<Doctor> getDoctorsInHospitalByLastName(Long hospitalId, String lastName) {
        List<Doctor> doctorsWithName = doctorRepository.findAllByHospital_IdAndUser_LastName(hospitalId, lastName);
        if (doctorsWithName.isEmpty()) {
            throw new DoctorNotFoundException("In this hospital not found doctors!");
        }
        return doctorsWithName;
    }

    @Override
    public List<Doctor> getDoctorsBySpecialityNameAndHospitalId(String specialityName, Long hospitalId) {
        List<Doctor> doctorsWithNameAndSpeciality = doctorRepository
                .findAllBySpeciality_NameAndHospital_Id(specialityName, hospitalId);
        if (doctorsWithNameAndSpeciality.isEmpty()) {
            throw new DoctorNotFoundException(String.format
                    ("Not found doctors with speciality name [%s] and hospital id [%s]!",
                            specialityName, hospitalId));
        }
        return doctorsWithNameAndSpeciality;
    }

    @Override
    public List<Doctor> getDoctorsBySpecialityIdAndHospitalIdAndUserLastName(Long specialityId,
                                                                             Long hospitalId, String lastName) {
        List<Doctor> doctorsInHospitalWithSpecialityAndName = doctorRepository
                .findAllBySpeciality_IdAndHospital_IdAndUser_LastName(specialityId, hospitalId, lastName);
        if (doctorsInHospitalWithSpecialityAndName.isEmpty()) {
            throw new DoctorNotFoundException(String.format
                    ("Not found doctors with speciality id [%s] and hospital id [%s] and name [%s]!",
                            specialityId, hospitalId, lastName));
        }
        return doctorsInHospitalWithSpecialityAndName;
    }

    @Override
    public List<DoctorSearchFormDto> getListDoctorsDto(List<Doctor> doctors) {
        return doctors.stream().map(this::getDoctorDto)
                .sorted(Comparator.comparing(DoctorSearchFormDto::getLastName))
                .collect(Collectors.toList());
    }

    private DoctorSearchFormDto getDoctorDto(Doctor doctor) {
        return DoctorSearchFormDto
                .builder()
                .id(doctor.getId())
                .cabinetNumber(doctor.getCabinetNumber())
                .firstName(doctor.getUser().getFirstName())
                .lastName(doctor.getUser().getLastName())
                .hospital(doctor.getHospital().getName())
                .speciality(doctor.getSpeciality().getName())
                .build();
    }
}

