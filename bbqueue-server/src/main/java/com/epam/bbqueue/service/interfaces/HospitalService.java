package com.epam.bbqueue.service.interfaces;

import com.epam.bbqueue.entity.Hospital;

import java.util.List;

public interface HospitalService {

    Hospital getHospitalByNameAndRegionId(String hospitalName, Long regionId);

    List<Hospital> getAllHospitalsByRegionId(Long regionId);

}
