package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.Region;
import com.epam.bbqueue.exception.RegionNotFoundException;
import com.epam.bbqueue.repository.RegionRepository;
import com.epam.bbqueue.service.interfaces.RegionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RegionServiceImpl implements RegionService {

    private final RegionRepository regionRepository;

    public RegionServiceImpl(RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

    @Override
    public Region getRegionByName(String regionName) {
        return regionRepository.findByNameIn(regionName)
                .orElseThrow(() -> new RegionNotFoundException(String.format
                        ("Not found the region with name [%s]", regionName)));
    }

    @Override
    public List<String> getAllRegionNames() {
        return regionRepository.findAll().stream().map(Region::getName).collect(Collectors.toList());
    }
}
