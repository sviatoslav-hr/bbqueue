package com.epam.bbqueue.service.interfaces;

import com.epam.bbqueue.entity.Region;

import java.util.List;

public interface RegionService {

    Region getRegionByName(String regionName);

    List<String> getAllRegionNames();
}
