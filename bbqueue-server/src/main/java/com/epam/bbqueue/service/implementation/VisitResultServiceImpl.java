package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.TimeSlot;
import com.epam.bbqueue.exception.ActiveQueuePlaceNotFoundException;
import com.epam.bbqueue.exception.HistoryOfVisitsForUserNotFoundException;
import com.epam.bbqueue.repository.TimeSlotRepository;
import com.epam.bbqueue.service.interfaces.VisitResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class VisitResultServiceImpl implements VisitResultService {

    private final TimeSlotRepository timeSlotRepository;

    @Autowired
    public VisitResultServiceImpl(TimeSlotRepository timeSlotRepository) {
        this.timeSlotRepository = timeSlotRepository;
    }

    @Override
    public List<TimeSlot> getAllHistoryOfVisitsForUser(Long idUser, Date date) {
        List<TimeSlot> allHistoryOfVisitsForUser = timeSlotRepository.findAllByCustomer_IdAndVisitDateLessThan(idUser, date);
        if (allHistoryOfVisitsForUser.isEmpty()) {
            throw new HistoryOfVisitsForUserNotFoundException("This user has no History Of Visits(userId = " + idUser + ")!");
        } else {
            return allHistoryOfVisitsForUser;
        }
    }

    @Override
    public List<TimeSlot> getAllActiveQueuePlace(Long idUser, Date date) {
        List<TimeSlot> allActiveQueuePlace = timeSlotRepository.findAllByCustomer_IdAndVisitDateGreaterThanEqual(idUser, date);
        if (allActiveQueuePlace.isEmpty()){
            throw new ActiveQueuePlaceNotFoundException("This user has no Active Queue Place(userId = " + idUser + ")!");
        } else {
            return allActiveQueuePlace;
        }
    }
}
