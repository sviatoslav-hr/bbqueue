package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.aop.annotation.Loggable;
import com.epam.bbqueue.config.security.jwt.JwtProvider;
import com.epam.bbqueue.dto.JwtResponse;
import com.epam.bbqueue.entity.Principal;
import com.epam.bbqueue.entity.User;
import com.epam.bbqueue.entity.VerificationToken;
import com.epam.bbqueue.exception.*;
import com.epam.bbqueue.repository.PrincipalRepository;
import com.epam.bbqueue.repository.UserRepository;
import com.epam.bbqueue.repository.VerificationTokenRepository;
import com.epam.bbqueue.service.interfaces.AuthenticationService;
import com.epam.bbqueue.service.interfaces.PrincipalService;
import com.epam.bbqueue.service.interfaces.SimpleEmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class AuthenticationServiceImpl implements AuthenticationService {
    private final PrincipalRepository principalRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtProvider jwtProvider;
    private final VerificationTokenRepository verificationTokenRepository;
    private final SimpleEmailService simpleEmailService;
    private final PrincipalService principalService;
    private final UserRepository userRepository;

    public AuthenticationServiceImpl(PrincipalRepository principalRepository,
                                     AuthenticationManager authenticationManager, JwtProvider jwtProvider,
                                     VerificationTokenRepository verificationTokenRepository,
                                     SimpleEmailService simpleEmailService, PrincipalService principalService,
                                     UserRepository userRepository) {
        this.principalRepository = principalRepository;
        this.authenticationManager = authenticationManager;
        this.jwtProvider = jwtProvider;
        this.verificationTokenRepository = verificationTokenRepository;
        this.simpleEmailService = simpleEmailService;
        this.principalService = principalService;
        this.userRepository = userRepository;
    }

    @Override
    @Loggable
    public JwtResponse attemptLogin(String username, String password) {
        if (!principalRepository.existsByEmail(username)) {
            throw new UserNotFoundException(
                    String.format("User [%s] does not exist", username));
        }
        Authentication authentication;
        authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(username, password));
        Principal principal = (Principal) authentication.getPrincipal();
        if (!principal.isActivated()) {
            throw new PrincipalNotActivatedException(
                    String.format("Email is not activated for user [%s]", username));
        }
        String token = jwtProvider.generateJwtToken(authentication);
        return new JwtResponse(token);
    }

    @Override
    @Loggable
    public void verifyEmail(String email, String code) {
        Principal principal = principalRepository.findByEmail(email).orElseThrow(() ->
                new UserNotFoundException(String.format("Not found user for email verification with email [%s]", email)));
        VerificationToken verificationToken = principal.getVerificationToken();
        if (verificationToken == null) {
            String message = String.format("No authentication token for email: [%s]", email);
            throw new EmptyVerificationTokenException(message);
        } else if (verificationToken.isConfirmed()) {
            String message = String.format("Can not confirm already confirmed email: [%s]", email);
            throw new AlreadyConfirmedVerificationTokenException(message);
        } else {
            Date now = new Date();
            if (now.after(verificationToken.getExpiredDateTime())) {
                String message = String.format("Confirmation time has already expired for email [%s]", email);
                throw new ExpiredVerificationTokenException(message);
            } else if (!verificationToken.getToken().equals(code)) {
                String message = String.format("Wrong confirmation code for email [%s]", email);
                throw new WrongConfirmationTokenException(message);
            } else {
                verificationToken.setConfirmed(true);
                verificationToken.setConfirmedDateTime(now);
                verificationTokenRepository.save(verificationToken);
                principal.setActivated(true);
                principalRepository.save(principal);
            }
        }
    }

    @Loggable
    @Override
    public void sendEmailVerificationCode(String email) {
        Principal principal = principalRepository.findByEmail(email).orElseThrow(() ->
                new UserNotFoundException(String.format("Not found user with email [%s]", email)));
        VerificationToken verificationToken = updateVerificationToken(principal);
        verificationTokenRepository.save(verificationToken);
        simpleEmailService.sendSimpleMessage(principal.getEmail(),
                "BBQueue email verification",
                verificationToken.getToken());
    }

    private VerificationToken updateVerificationToken(Principal principal) {
        Calendar calendar = Calendar.getInstance();
        Date issuedDateTime = calendar.getTime();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date expiredDateTime = calendar.getTime();
        return VerificationToken.builder()
                .id(principal.getVerificationToken() != null ? principal.getVerificationToken().getId() : null)
                .token(UUID.randomUUID().toString())
                .isConfirmed(false)
                .principal(principal)
                .issuedDateTime(issuedDateTime)
                .expiredDateTime(expiredDateTime)
                .build();
    }

    @Override
    public void changePassword(String email, String password, String code) {
        Principal principal = principalRepository.findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException(String.format("Not found user with email [%s]", email)));
        VerificationToken verificationToken = principal.getVerificationToken();
        if (verificationToken == null) {
            String message = String.format("No authentication token for email: [%s]", email);
            throw new EmptyVerificationTokenException(message);
        } else if (verificationToken.isConfirmed()) {
            String message = String.format("Can not confirm already confirmed email: [%s]", email);
            throw new AlreadyConfirmedVerificationTokenException(message);
        } else {
            Date now = new Date();
            if (now.after(verificationToken.getExpiredDateTime())) {
                String message = String.format("Confirmation time has already expired for email [%s]", email);
                throw new ExpiredVerificationTokenException(message);
            } else if (!verificationToken.getToken().equals(code)) {
                String message = String.format("Wrong confirmation code for email [%s]", email);
                throw new WrongConfirmationTokenException(message);
            } else {
                verificationToken.setConfirmed(true);
                verificationToken.setConfirmedDateTime(now);
                principalService.setNewPassword(principal.getEmail(), password);
                verificationTokenRepository.save(verificationToken);
            }
        }
    }

    @Override
    public Optional<User> getAuthenticationUser(Long idPrincipal) {
        return userRepository.findByPrincipalId(idPrincipal);
    }
}
