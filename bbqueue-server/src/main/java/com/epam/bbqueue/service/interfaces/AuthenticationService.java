package com.epam.bbqueue.service.interfaces;

import com.epam.bbqueue.dto.JwtResponse;
import com.epam.bbqueue.entity.User;

import java.util.Optional;

public interface AuthenticationService {
    JwtResponse attemptLogin(String username, String password);
    void verifyEmail(String email, String code);
    void sendEmailVerificationCode(String email);
    void changePassword(String email, String password, String code);
    Optional<User> getAuthenticationUser(Long idPrincipal);
}
