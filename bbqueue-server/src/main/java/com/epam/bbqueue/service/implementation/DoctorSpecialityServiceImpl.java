package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.Doctor;
import com.epam.bbqueue.entity.DoctorSpeciality;
import com.epam.bbqueue.exception.DoctorSpecialityNotFoundException;
import com.epam.bbqueue.repository.DoctorRepository;
import com.epam.bbqueue.repository.DoctorSpecialityRepository;
import com.epam.bbqueue.service.interfaces.DoctorSpecialityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DoctorSpecialityServiceImpl implements DoctorSpecialityService {

    private final DoctorSpecialityRepository doctorSpecialityRepository;
    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorSpecialityServiceImpl(
            DoctorSpecialityRepository doctorSpecialityRepository, DoctorRepository doctorRepository) {
        this.doctorSpecialityRepository = doctorSpecialityRepository;
        this.doctorRepository = doctorRepository;
    }

    @Override
    public DoctorSpeciality getDoctorSpecialityByName(String specialityName) {
        return doctorSpecialityRepository.findByName(specialityName)
                .orElseThrow(() -> new DoctorSpecialityNotFoundException(String.format
                        ("Not found the speciality with name [%s]", specialityName)));
    }

    @Override
    public List<String> getDoctorSpecialitiesByHospital(Long hospitalId) {
        List<Doctor> doctorsInHospital = doctorRepository.findAllByHospital_Id(hospitalId);
        List<String> specialityNamesInHospital = doctorsInHospital.stream()
                .map(Doctor::getSpeciality)
                .map(DoctorSpeciality::getName).distinct().collect(Collectors.toList());
        if (specialityNamesInHospital.isEmpty()) {
            throw new DoctorSpecialityNotFoundException("Not found the specialities in this hospital");
        }
        return specialityNamesInHospital;
    }
}
