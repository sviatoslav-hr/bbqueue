package com.epam.bbqueue.service.interfaces;

import com.epam.bbqueue.dto.DoctorSearchFormDto;
import com.epam.bbqueue.entity.Doctor;

import java.util.List;

public interface DoctorService {

    List<Doctor> getDoctorsByHospitalId(Long hospitalId);

    List<Doctor> getDoctorsInHospitalByLastName(Long hospitalId, String lastName);

    List<Doctor> getDoctorsBySpecialityNameAndHospitalId(String specialityName, Long hospitalId);

    List<Doctor> getDoctorsBySpecialityIdAndHospitalIdAndUserLastName(Long specialityId, Long hospitalId,
                                                                      String lastName);

    List<DoctorSearchFormDto> getListDoctorsDto(List<Doctor> doctors);

}
