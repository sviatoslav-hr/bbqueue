package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.Hospital;
import com.epam.bbqueue.exception.HospitalNotFoundException;
import com.epam.bbqueue.repository.HospitalRepository;
import com.epam.bbqueue.service.interfaces.HospitalService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HospitalServiceImpl implements HospitalService {

    private final HospitalRepository hospitalRepository;

    public HospitalServiceImpl(HospitalRepository hospitalRepository) {
        this.hospitalRepository = hospitalRepository;
    }

    @Override
    public Hospital getHospitalByNameAndRegionId(String hospitalName, Long regionId) {
        return hospitalRepository.findByNameAndRegion_Id(hospitalName, regionId)
                .orElseThrow(() -> new HospitalNotFoundException(String.format
                        ("Not found the hospital with name [%s]", hospitalName)));
    }

    @Override
    public List<Hospital> getAllHospitalsByRegionId(Long regionId) {
        return hospitalRepository.findAllByRegion_Id(regionId);
    }
}
