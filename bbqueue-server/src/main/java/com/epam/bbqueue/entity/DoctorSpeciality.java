package com.epam.bbqueue.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@NamedEntityGraph(
        name = "doctorSpeciality-doctors-graph",
        attributeNodes = {
                @NamedAttributeNode("doctors")
        }
)
public class DoctorSpeciality {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(mappedBy = "speciality")
    private Set<Doctor> doctors;
}
