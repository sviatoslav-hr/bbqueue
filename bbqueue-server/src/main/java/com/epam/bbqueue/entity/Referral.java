package com.epam.bbqueue.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NamedEntityGraph(
        name = "referral-user-graph",
        attributeNodes = {
                @NamedAttributeNode("customer")
        }
)
public class Referral {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String description;

    @ManyToOne
    @JoinColumn(name = "doctor_from_id", nullable = false)
    private Doctor doctorFrom;

    @ManyToOne
    @JoinColumn(name = "doctor_to_id", nullable = false)
    private Doctor doctorTo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", nullable = false)
    private User customer;

    @Temporal(TemporalType.DATE)
    @Column(name = "expiration_date")
    private Date expirationDate;
}
