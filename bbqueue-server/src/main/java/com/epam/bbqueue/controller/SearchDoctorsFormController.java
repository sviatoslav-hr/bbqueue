package com.epam.bbqueue.controller;

import com.epam.bbqueue.service.interfaces.DoctorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/doctors")
public class SearchDoctorsFormController {

    private final DoctorService doctorService;

    public SearchDoctorsFormController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping("/in-hospital/{hospitalId}")
    public ResponseEntity getDoctorsByHospital(@PathVariable Long hospitalId) {
        return ResponseEntity.ok(
                doctorService.getListDoctorsDto(
                        doctorService.getDoctorsByHospitalId(hospitalId)));
    }

    @GetMapping("/with-speciality")
    public ResponseEntity getDoctorsBySpecialityAndHospital(@RequestParam String specialityName, Long hospitalId) {
        return ResponseEntity.ok(
                doctorService.getListDoctorsDto(
                        doctorService.getDoctorsBySpecialityNameAndHospitalId(specialityName, hospitalId)));
    }

    @GetMapping("/with-speciality-and-name")
    public ResponseEntity getDoctorsBySpecialityAndHospitalAndDoctorName(@RequestParam Long specialityId,
                                                                         Long hospitalId, String lastName) {
        return ResponseEntity.ok(
                doctorService.getListDoctorsDto(
                        doctorService.getDoctorsBySpecialityIdAndHospitalIdAndUserLastName(specialityId,
                                                                                            hospitalId,
                                                                                            lastName)));
    }

    @GetMapping("by-name")
    public ResponseEntity getDoctorsByHospitalAndDoctorName(@RequestParam Long hospitalId, String lastName) {
        return ResponseEntity.ok(
                doctorService.getListDoctorsDto(
                        doctorService.getDoctorsInHospitalByLastName(hospitalId, lastName)));
    }
}
