package com.epam.bbqueue.controller;

import com.epam.bbqueue.aop.annotation.Loggable;
import com.epam.bbqueue.dto.ReferralDto;
import com.epam.bbqueue.entity.Referral;
import com.epam.bbqueue.service.interfaces.ReferralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/user")
public class ReferralController {

    private final ReferralService referralService;
    private final AuthenticationController authenticationController;

    @Autowired
    public ReferralController(ReferralService referralService,
                              AuthenticationController authenticationController) {
        this.referralService = referralService;
        this.authenticationController = authenticationController;
    }

    @Loggable
    @GetMapping("/referral")
    public ResponseEntity getReferralForUser(){
        Long idUser = authenticationController.getAuthenticationUser().getId();
        List<Referral> allReferralOfUser = referralService.getAllReferralOfUser(idUser);
        List<ReferralDto> allReferralDtoOfUser = allReferralOfUser.stream().map(this::referralConvertReferralDto).collect(Collectors.toList());
        return ResponseEntity.ok(allReferralDtoOfUser);
    }

    private ReferralDto referralConvertReferralDto(Referral referral){
        return ReferralDto.builder()
                .id(referral.getId())
                .description(referral.getDescription())
                .doctorFromName(referral.getDoctorFrom().getUser().getFirstName()
                        + " " + referral.getDoctorFrom().getUser().getLastName())
                .doctorToName(referral.getDoctorTo().getUser().getFirstName()
                        + " " + referral.getDoctorTo().getUser().getLastName())
                .customerName(referral.getCustomer().getFirstName()
                        + " " + referral.getCustomer().getLastName())
                .expirationDate(referral.getExpirationDate())
                .build();
    }
}
