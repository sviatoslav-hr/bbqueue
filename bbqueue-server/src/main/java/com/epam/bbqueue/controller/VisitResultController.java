package com.epam.bbqueue.controller;

import com.epam.bbqueue.aop.annotation.Loggable;
import com.epam.bbqueue.entity.TimeSlot;
import com.epam.bbqueue.service.interfaces.VisitResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/user")
public class VisitResultController {

    private final VisitResultService timeSlotService;
    private final AuthenticationController authenticationController;

    @Autowired
    public VisitResultController(VisitResultService timeSlotService,
                                 AuthenticationController authenticationController) {
        this.timeSlotService = timeSlotService;
        this.authenticationController = authenticationController;
    }

    @Loggable
    @GetMapping("/visits-history")
    public ResponseEntity getAllHistoryOfVisitsForUser(){
        Long idUser = authenticationController.getAuthenticationUser().getId();
        return ResponseEntity.ok(timeSlotService.getAllHistoryOfVisitsForUser(idUser, new Date()));
    }

    @Loggable
    @GetMapping("/visits-active")
    public ResponseEntity getAllActiveQueuePlace(){
        Long idUser = authenticationController.getAuthenticationUser().getId();
        return ResponseEntity.ok(timeSlotService.getAllActiveQueuePlace(idUser, new Date()));
    }
}
