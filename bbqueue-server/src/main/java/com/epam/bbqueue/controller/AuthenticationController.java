package com.epam.bbqueue.controller;

import com.epam.bbqueue.dto.*;
import com.epam.bbqueue.entity.Principal;
import com.epam.bbqueue.entity.User;
import com.epam.bbqueue.exception.UserNotFoundException;
import com.epam.bbqueue.service.interfaces.AuthenticationService;
import com.epam.bbqueue.service.interfaces.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/authentication")
public class AuthenticationController {
    private final AuthenticationService authenticationService;
    private final UserService userService;

    public AuthenticationController(AuthenticationService authenticationService, UserService userService) {
        this.authenticationService = authenticationService;
        this.userService = userService;
    }

    @PostMapping("/sign-in")
    public JwtResponse signIn(@RequestBody SignInDto signInDto) {
        return authenticationService.attemptLogin(signInDto.getEmail(), signInDto.getPassword());
    }

    @PostMapping("/sign-up")
    public void signUp(@RequestBody SignUpDto signUpDto) {
        userService.save(
                User.builder()
                        .firstName(signUpDto.getFirstName())
                        .lastName(signUpDto.getLastName())
                        .principal(
                                Principal.builder()
                                        .email(signUpDto.getEmail())
                                        .password(signUpDto.getPassword())
                                        .build()
                        ).build());
    }

    @PostMapping("/confirm-email")
    public void confirmEmail(@RequestBody EmailVerificationDto verificationDto) {
        authenticationService.verifyEmail(verificationDto.getEmail(), verificationDto.getCode());
    }

    @PostMapping("/send-verification-code")
    public void sendVerificationCode(@RequestBody SendVerificationCodeDto dto) {
        authenticationService.sendEmailVerificationCode(dto.getEmail());
    }

    @PostMapping("/change-password")
    public void changePassword(@RequestBody ChangePasswordDto dto) {
        authenticationService.changePassword(dto.getEmail(), dto.getPassword(), dto.getCode());
    }

    @RequestMapping
    public User getAuthenticationUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Principal principal = (Principal) authentication.getPrincipal();
        return authenticationService.getAuthenticationUser(principal.getId())
                .orElseThrow(() -> new UserNotFoundException("There is no such user"));
    }
}
