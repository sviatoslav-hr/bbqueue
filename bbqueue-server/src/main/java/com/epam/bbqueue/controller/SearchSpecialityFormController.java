package com.epam.bbqueue.controller;

import com.epam.bbqueue.service.interfaces.DoctorSpecialityService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/speciality")
public class SearchSpecialityFormController {

    private final DoctorSpecialityService doctorSpecialityService;

    public SearchSpecialityFormController(DoctorSpecialityService doctorSpecialityService) {
        this.doctorSpecialityService = doctorSpecialityService;
    }

    @GetMapping("/{specialityName}")
    public ResponseEntity getSpecialityByName(@PathVariable String specialityName) {
        return ResponseEntity.ok(doctorSpecialityService.getDoctorSpecialityByName(specialityName));
    }

    @GetMapping("/in-hospital/{hospitalId}")
    public ResponseEntity getDoctorSpecialitiesInHospital(@PathVariable Long hospitalId) {
        return ResponseEntity.ok(doctorSpecialityService.getDoctorSpecialitiesByHospital(hospitalId));
    }
}