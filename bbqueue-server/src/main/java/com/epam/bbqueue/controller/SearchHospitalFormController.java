package com.epam.bbqueue.controller;

import com.epam.bbqueue.entity.Hospital;
import com.epam.bbqueue.service.interfaces.HospitalService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/hospital")
public class SearchHospitalFormController {

    private final HospitalService hospitalService;

    public SearchHospitalFormController(HospitalService hospitalService) {
        this.hospitalService = hospitalService;
    }

    @GetMapping
    public ResponseEntity getHospitalByNameAndRegion(@RequestParam String hospitalName, @RequestParam Long regionId) {
        return ResponseEntity.ok(hospitalService.getHospitalByNameAndRegionId(hospitalName, regionId));
    }

    @GetMapping("/all/{regionId}")
    public ResponseEntity getAllHospitalNamesInRegion(@PathVariable Long regionId) {
        return ResponseEntity.ok(hospitalService.getAllHospitalsByRegionId(regionId)
                .stream().map(Hospital::getName).collect(Collectors.toList()));
    }
}
