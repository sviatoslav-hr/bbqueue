package com.epam.bbqueue.controller;

import com.epam.bbqueue.service.interfaces.RegionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/region")
public class SearchRegionFormController {

    private final RegionService regionService;

    public SearchRegionFormController(RegionService regionService) {
        this.regionService = regionService;
    }

    @GetMapping("/{regionName}")
    public ResponseEntity getRegionByName(@PathVariable String regionName) {
        return ResponseEntity.ok(regionService.getRegionByName(regionName));
    }

    @GetMapping("/all")
    public ResponseEntity getAllRegionNames() {
        return ResponseEntity.ok(regionService.getAllRegionNames());
    }
}
