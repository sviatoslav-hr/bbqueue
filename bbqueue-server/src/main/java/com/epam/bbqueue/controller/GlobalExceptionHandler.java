package com.epam.bbqueue.controller;

import com.epam.bbqueue.exception.*;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.URI;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(UserNotFoundException.class)
    ResponseEntity<String> handleUserNotFoundException(UserNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
    }

    @ExceptionHandler(PrincipalNotActivatedException.class)
    ResponseEntity<String> handleUserNotFoundException(PrincipalNotActivatedException ex) {
        return ResponseEntity.status(HttpStatus.LOCKED).body(ex.getMessage());
    }

    @ExceptionHandler(InvalidPrincipalException.class)
    ResponseEntity<String> handleInvalidPrincipalException(InvalidPrincipalException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(OccupiedEmailException.class)
    ResponseEntity<String> handleOccupiedEmailException(OccupiedEmailException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(ex.getMessage());
    }

    @ExceptionHandler(InvalidUserDetailsException.class)
    ResponseEntity<String> handleInvalidUserDetailsException(InvalidUserDetailsException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(EmptyVerificationTokenException.class)
    ResponseEntity<String> handleEmptyVerificationTokenException(EmptyVerificationTokenException ex) {
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

    @ExceptionHandler(ExpiredVerificationTokenException.class)
    ResponseEntity<String> handleExpiredVerificationTokenException(ExpiredVerificationTokenException ex) {
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

    @ExceptionHandler(AlreadyConfirmedVerificationTokenException.class)
    ResponseEntity<String> handleNotConfirmedVerificationTokenException(AlreadyConfirmedVerificationTokenException ex) {
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

    @ExceptionHandler(WrongConfirmationTokenException.class)
    ResponseEntity<String> handleWrongConfirmationTokenException(WrongConfirmationTokenException ex) {
        return ResponseEntity
                .unprocessableEntity()
                .body(ex.getMessage());
    }

    @ExceptionHandler(DoctorNotFoundException.class)
    ResponseEntity<String> handleDoctorNotFoundException(DoctorNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
    }

    @ExceptionHandler(RegionNotFoundException.class)
    ResponseEntity<String> handleRegionNotFoundException(RegionNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
    }

    @ExceptionHandler(HospitalNotFoundException.class)
    ResponseEntity<String> handleHospitalNotFoundException(HospitalNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
    }

    @ExceptionHandler(DoctorSpecialityNotFoundException.class)
    ResponseEntity<String> handleDoctorSpecialityNotFoundException(DoctorSpecialityNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
    }

    @ExceptionHandler(DoctorWorkDaysNotFoundException.class)
    ResponseEntity<String> handleDoctorWorkDaysNotFoundException(DoctorWorkDaysNotFoundException ex) {
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

    @ExceptionHandler(ReferralNotFoundException.class)
    ResponseEntity<String> handleReferralNotFoundException(ReferralNotFoundException ex){
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

    @ExceptionHandler(HistoryOfVisitsForUserNotFoundException.class)
    ResponseEntity<String> handleHistoryOfVisitsForUserNotFoundException(HistoryOfVisitsForUserNotFoundException ex){
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

    @ExceptionHandler(ActiveQueuePlaceNotFoundException.class)
    ResponseEntity<String> handleActiveQueuePlaceNotFoundException(ActiveQueuePlaceNotFoundException ex){
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status.value());
        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
        body.put("errors", errors);
        return new ResponseEntity<>(body, headers, status);
    }
}
