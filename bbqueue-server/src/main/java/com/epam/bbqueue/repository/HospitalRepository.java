package com.epam.bbqueue.repository;

import com.epam.bbqueue.entity.Hospital;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HospitalRepository extends JpaRepository<Hospital, Long> {

    @EntityGraph(value = "hospital-region-graph", type = EntityGraph.EntityGraphType.FETCH)
    Optional<Hospital> findByNameAndRegion_Id(String hospitalName, Long regionId);

    List<Hospital> findAllByRegion_Id(Long regionId);

}
