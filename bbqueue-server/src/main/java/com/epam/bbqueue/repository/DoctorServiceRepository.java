package com.epam.bbqueue.repository;

import com.epam.bbqueue.entity.DoctorService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DoctorServiceRepository extends JpaRepository<DoctorService, Long> {

    Optional<DoctorService> findByName(String name);
}
