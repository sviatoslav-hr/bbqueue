package com.epam.bbqueue.repository;

import com.epam.bbqueue.entity.Doctor;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {

    @EntityGraph(value = "doctor-search-form-graph", type = EntityGraph.EntityGraphType.FETCH)
    List<Doctor> findAllByHospital_Id(Long hospitalId);

    @EntityGraph(value = "doctor-search-form-graph", type = EntityGraph.EntityGraphType.FETCH)
    List<Doctor> findAllBySpeciality_NameAndHospital_Id(String specialityName, Long hospitalId);

    @EntityGraph(value = "doctor-search-form-graph", type = EntityGraph.EntityGraphType.FETCH)
    List<Doctor> findAllByHospital_IdAndUser_LastName(Long hospitalId, String lastName);

    @EntityGraph(value = "doctor-search-form-graph", type = EntityGraph.EntityGraphType.FETCH)
    List<Doctor> findAllBySpeciality_IdAndHospital_IdAndUser_LastName(Long specialityId, Long hospitalId,
                                                                      String lastName);
}
