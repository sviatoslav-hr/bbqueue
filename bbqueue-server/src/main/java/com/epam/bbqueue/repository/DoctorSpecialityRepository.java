package com.epam.bbqueue.repository;

import com.epam.bbqueue.entity.DoctorSpeciality;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DoctorSpecialityRepository extends JpaRepository<DoctorSpeciality, Long> {

    Optional<DoctorSpeciality> findByName(String specialityName);

}
