package com.epam.bbqueue.repository;

import com.epam.bbqueue.entity.Referral;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReferralRepository extends JpaRepository<Referral, Long> {

    @EntityGraph(value = "referral-user-graph", type = EntityGraph.EntityGraphType.LOAD)
    List<Referral> findAllByCustomer_Id(Long id);

}
