package com.epam.bbqueue.exception;

public class HistoryOfVisitsForUserNotFoundException extends RuntimeException {
    public HistoryOfVisitsForUserNotFoundException(String message){
        super(message);
    }
}