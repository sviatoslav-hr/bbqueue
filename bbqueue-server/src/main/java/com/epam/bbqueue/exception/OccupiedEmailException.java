package com.epam.bbqueue.exception;

public class OccupiedEmailException extends RuntimeException {
    public OccupiedEmailException(String message) {
        super(message);
    }
}
