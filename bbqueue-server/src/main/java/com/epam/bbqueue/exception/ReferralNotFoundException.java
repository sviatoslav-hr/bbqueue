package com.epam.bbqueue.exception;

public class ReferralNotFoundException extends RuntimeException {
    public ReferralNotFoundException(String message){
        super(message);
    }
}
