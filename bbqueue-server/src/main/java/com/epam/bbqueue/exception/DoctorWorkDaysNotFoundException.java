package com.epam.bbqueue.exception;

public class DoctorWorkDaysNotFoundException extends RuntimeException {
  public DoctorWorkDaysNotFoundException(String message) {
    super(message);
  }
}
