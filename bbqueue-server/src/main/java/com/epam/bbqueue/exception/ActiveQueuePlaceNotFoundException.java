package com.epam.bbqueue.exception;

public class ActiveQueuePlaceNotFoundException extends RuntimeException {
    public ActiveQueuePlaceNotFoundException(String message){
        super(message);
    }
}