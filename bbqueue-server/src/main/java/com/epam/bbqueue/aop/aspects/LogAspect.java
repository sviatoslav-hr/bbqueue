package com.epam.bbqueue.aop.aspects;

import com.epam.bbqueue.aop.annotation.Loggable;
import com.epam.bbqueue.aop.interfaces.Printable;
import com.epam.bbqueue.entity.enums.LogLevels;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Aspect
@Component
@Slf4j
public class LogAspect {

    private Map<LogLevels, Printable> logsLevel;

    public LogAspect() {
        System.out.println("init");
        logsLevel = new HashMap<>();
        logsLevel.put(LogLevels.TRACE, log::trace);
        logsLevel.put(LogLevels.DEBUG, log::debug);
        logsLevel.put(LogLevels.WARN, log::warn);
        logsLevel.put(LogLevels.INFO, log::info);
        logsLevel.put(LogLevels.ERROR, log::error);
    }

    @Pointcut("@annotation(com.epam.bbqueue.aop.annotation.Loggable)")
    public void loggablePointCut() {
    }

    @Pointcut("within(com.epam.bbqueue.controller..*)")
    public void controllerPointCut() {
    }

    @Pointcut("within(com.epam.bbqueue.service.implementation..*)")
    public void servicePointCut() {
    }

    @Before("loggablePointCut() && (controllerPointCut() || servicePointCut())")
    public void beforeCallMethod(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Loggable annotation = signature.getMethod().getAnnotation(Loggable.class);
        if (!annotation.message().isEmpty()) {
            logsLevel.getOrDefault(annotation.level(), log::debug).print(annotation.message());
        } else {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder
                    .append("\nCalling method - ")
                    .append(joinPoint.getSignature())
                    .append("\nWith parameters - ");
            Arrays.stream(joinPoint.getArgs()).forEach(param -> stringBuilder
                    .append(param.getClass().getSimpleName())
                    .append(" : ")
                    .append(param.toString())
                    .append(" | "));
            logsLevel.getOrDefault(annotation.level(), log::debug).print(stringBuilder.toString());
        }
    }
}
