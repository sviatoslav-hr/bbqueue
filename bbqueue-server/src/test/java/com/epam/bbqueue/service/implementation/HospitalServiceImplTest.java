package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.Hospital;
import com.epam.bbqueue.repository.HospitalRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeFalse;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {HospitalServiceImpl.class})
public class HospitalServiceImplTest {

    @MockBean
    private HospitalRepository hospitalRepository;

    @Autowired
    private HospitalServiceImpl hospitalService;

    private Hospital actualResult;
    private List<Hospital> expectedList;
    private Hospital expectedResult;

    @Before
    public void init() {
        expectedResult = new Hospital();
        expectedResult.setName("Hospital");
        expectedList = Arrays.asList(expectedResult);

        when(hospitalRepository.findAllByRegion_Id(1L)).thenReturn(expectedList);

        when(hospitalRepository.findByNameAndRegion_Id(expectedResult.getName(), 1L))
                .thenReturn(Optional.of(expectedResult));

    }


    @Test
    public void getHospitalByNameAndRegion() {

        actualResult = hospitalService.getHospitalByNameAndRegionId("Hospital", 1L);

        assertEquals(expectedResult, actualResult);

    }

    @Test
    public void getAllHospitals() {

        List<Hospital> actualList = hospitalService.getAllHospitalsByRegionId(1L);

        assumeFalse(actualList.isEmpty());
        assertEquals(1, actualList.size());
        assertThat(actualList).isEqualTo(expectedList);
    }

}