package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.DoctorService;
import com.epam.bbqueue.entity.TimeSlot;
import com.epam.bbqueue.entity.User;
import com.epam.bbqueue.entity.VisitResult;
import com.epam.bbqueue.repository.TimeSlotRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {VisitResultServiceImpl.class})
@Slf4j
public class VisitResultServiceImplTest {

    @Autowired
    private VisitResultServiceImpl visitResultService;

    @MockBean
    private TimeSlotRepository timeSlotRepository;

    @Before
    public void initHistoryOfVisitsForUser(){
        when(timeSlotRepository.findAllByCustomer_IdAndVisitDateLessThan(1L, new Date(2019-9-25)))
                .thenReturn(Arrays.asList(
                   createTimeSlot(1,new Date(2019-9-15)),
                   createTimeSlot(4,new Date(2019-9-5)),
                   createTimeSlot(5,new Date(2019-9-13)),
                   createTimeSlot(9,new Date(2019-9-9)),
                   createTimeSlot(13,new Date(2019-9-2)),
                   createTimeSlot(15,new Date(2019-9-24))));

        when(timeSlotRepository.saveAndFlush(createTimeSlot(6, new Date(2019-9-22))))
                .thenReturn(createTimeSlot(6,new Date(2019-9-22)));
    }

    @Before
    public void initActiveQueuePlace(){
        when(timeSlotRepository.findAllByCustomer_IdAndVisitDateGreaterThanEqual(2L, new Date(2019-9-25)))
                .thenReturn(Arrays.asList(
                        createTimeSlot(2,new Date(2019-9-26)),
                        createTimeSlot(9,new Date(2019-9-29)),
                        createTimeSlot(11,new Date(2019-10-2)),
                        createTimeSlot(12,new Date(2019-10-14)),
                        createTimeSlot(16,new Date(2019-9-28)),
                        createTimeSlot(19,new Date(2019-11-6))));

        when(timeSlotRepository.saveAndFlush(createTimeSlot(14, new Date(2019-9-28))))
                .thenReturn(createTimeSlot(14,new Date(2019-9-28)));
    }

    private TimeSlot createTimeSlot(Integer timeSlotNumber, Date visitDate){
        TimeSlot timeSlot = new TimeSlot();
        User user = new User();
        DoctorService service = new DoctorService();
        VisitResult visitResult = new VisitResult();

        timeSlot.setId(1L);
        timeSlot.setVisitDate(visitDate);
        timeSlot.setTimeSlotNumber(timeSlotNumber);
        timeSlot.setCustomer(user);
        timeSlot.setService(service);
        timeSlot.setVisitResult(visitResult);
        return timeSlot;
    }

    @Test
    public void testGetAllHistoryOfVisitsForUser() {
        List<TimeSlot> historyOfVisitsForUser = visitResultService.getAllHistoryOfVisitsForUser(1L, new Date(2019-9-25));
        Assert.assertEquals(6,historyOfVisitsForUser.size());
        Assert.assertEquals(new Integer(4), historyOfVisitsForUser.get(1).getTimeSlotNumber());
        Assert.assertEquals(new Date(2019-9-24), historyOfVisitsForUser.get(5).getVisitDate());
    }

    @Test
    public void testGetAllActiveQueuePlace() {
        List<TimeSlot> activeQueuePlace = visitResultService.getAllActiveQueuePlace(2L, new Date(2019-9-25));
        Assert.assertEquals(6, activeQueuePlace.size());
        Assert.assertEquals(new Integer(9), activeQueuePlace.get(1).getTimeSlotNumber());
        Assert.assertEquals(new Date(2019-9-28), activeQueuePlace.get(4).getVisitDate());
    }
}
