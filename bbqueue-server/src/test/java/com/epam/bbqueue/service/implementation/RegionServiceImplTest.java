package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.Region;
import com.epam.bbqueue.repository.RegionRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {RegionServiceImpl.class})
public class RegionServiceImplTest {

    @MockBean
    private RegionRepository regionRepository;

    @Autowired
    private RegionServiceImpl regionService;

    private Region actualResult;
    private Region expectedResult;

    @Before
    public void init() {
        expectedResult = new Region();
        expectedResult.setName("Region");

        when(regionRepository.findByNameIn(expectedResult.getName()))
                .thenReturn(Optional.of(expectedResult));

    }


    @Test
    public void getRegionByName() {

        actualResult = regionService.getRegionByName("Region");

        assertEquals(expectedResult, actualResult);

    }
}