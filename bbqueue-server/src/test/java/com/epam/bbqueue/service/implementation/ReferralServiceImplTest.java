package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.Doctor;
import com.epam.bbqueue.entity.Referral;
import com.epam.bbqueue.entity.User;
import com.epam.bbqueue.repository.ReferralRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ReferralServiceImpl.class})
@Slf4j
public class ReferralServiceImplTest {

    @Autowired
    private ReferralServiceImpl referralService;

    @MockBean
    private ReferralRepository referralRepository;

    @Before
    public void initReferralRepository(){
        when(referralRepository.findAllByCustomer_Id(1L))
                .thenReturn(Arrays.asList(
                        createReferral("ultrasonography",new Date(2019-9-16)),
                        createReferral("x-ray",new Date(2019-9-21)),
                        createReferral("delivery of the analysis", new Date(2019-9-26))));

        when(referralRepository.saveAndFlush(createReferral("ultrasonography", new Date(2019-10-22))))
                .thenReturn(createReferral("ultrasonography", new Date(2019-10-22)));
    }

    private Referral createReferral(String description, Date expirationDate){
        Referral referral = new Referral();
        Doctor doctorFrom = new Doctor();
        doctorFrom.setId(5L);
        Doctor doctorTo = new Doctor();
        doctorTo.setId(6L);
        User user = new User();
        user.setId(1L);
        referral.setId(1L);
        referral.setDescription(description);
        referral.setDoctorFrom(doctorFrom);
        referral.setDoctorTo(doctorTo);
        referral.setCustomer(user);
        referral.setExpirationDate(expirationDate);
        return referral;
    }

    @Test
    public void testGetAllReferralOfUser(){
        List<Referral> referrals = referralService.getAllReferralOfUser(1L);
        Assert.assertEquals(3,referrals.size());
        Assert.assertEquals("ultrasonography", referrals.get(0).getDescription());
        Assert.assertEquals(new Date(2019-9-21), referrals.get(1).getExpirationDate());
    }
}
