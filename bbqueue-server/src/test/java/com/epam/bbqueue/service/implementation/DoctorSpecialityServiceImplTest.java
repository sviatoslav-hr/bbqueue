package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.DoctorSpeciality;
import com.epam.bbqueue.repository.DoctorRepository;
import com.epam.bbqueue.repository.DoctorSpecialityRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {DoctorSpecialityServiceImpl.class})
public class DoctorSpecialityServiceImplTest {

    @MockBean
    private DoctorSpecialityRepository specialityRepository;

    @MockBean
    private DoctorRepository doctorRepository;

    @Autowired
    private DoctorSpecialityServiceImpl specialityService;

    private DoctorSpeciality expectedResult;
    private DoctorSpeciality actualResult;

    @Before
    public void init() {
        expectedResult = new DoctorSpeciality();
        expectedResult.setName("Speciality");

        when(specialityRepository.findByName(expectedResult.getName()))
                .thenReturn(Optional.of(expectedResult));

    }


    @Test
    public void getDoctorSpecialityByName() {

        actualResult = specialityService.getDoctorSpecialityByName("Speciality");

        assertEquals(expectedResult, actualResult);

    }
}