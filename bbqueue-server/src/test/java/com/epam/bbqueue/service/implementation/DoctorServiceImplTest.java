package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.Doctor;
import com.epam.bbqueue.repository.DoctorRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeFalse;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {DoctorServiceImpl.class})
public class DoctorServiceImplTest {

    @MockBean
    private DoctorRepository doctorRepository;

    @Autowired
    private DoctorServiceImpl doctorServiceImpl;

    private List<Doctor> actualResult;
    private List<Doctor> expectedResult;

    @Before
    public void init() {
        Doctor doc = new Doctor();
        doc.setId(1L);
        expectedResult = Arrays.asList(doc);

        when(doctorRepository.findAllByHospital_Id(1L))
                .thenReturn(expectedResult);

        when(doctorRepository.findAllByHospital_IdAndUser_LastName(1L, "Bolit"))
                .thenReturn(expectedResult);

        when(doctorRepository
                .findAllBySpeciality_IdAndHospital_IdAndUser_LastName(1L, 1L, "Bolit"))
                .thenReturn(expectedResult);

        when(doctorRepository
                .findAllBySpeciality_NameAndHospital_Id("Family doctor", 1L))
                .thenReturn(expectedResult);
    }

    @Test
    public void testGetAllDoctorsByHospital() {

        actualResult = doctorServiceImpl.getDoctorsByHospitalId(1L);

        assumeFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());
        assertThat(actualResult).isEqualTo(expectedResult);
    }

    @Test
    public void testGetAllDoctorsInHospitalAndName() {

        actualResult = doctorServiceImpl.getDoctorsInHospitalByLastName(1L, "Bolit");

        assumeFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());
        assertThat(actualResult).isEqualTo(expectedResult);
    }

    @Test
    public void testGetAllDoctorsWithSpecialityAndName() {

        actualResult = doctorServiceImpl
                .getDoctorsBySpecialityIdAndHospitalIdAndUserLastName(1L, 1L, "Bolit");

        assumeFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());
        assertThat(actualResult).isEqualTo(expectedResult);
    }

    @Test
    public void testGetAllDoctorsInHospitalWithSpeciality() {

        actualResult = doctorServiceImpl
                .getDoctorsBySpecialityNameAndHospitalId("Family doctor", 1L);

        assumeFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());
        assertThat(actualResult).isEqualTo(expectedResult);
    }
}