package com.epam.bbqueue.controller;

import com.epam.bbqueue.entity.Hospital;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("deprecation")
@RunWith(SpringRunner.class)
@WebMvcTest(secure = false, value = {SearchHospitalFormController.class})
public class SearchHospitalFormControllerTest {

    @MockBean
    private SearchHospitalFormController controller;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void init() {

        when(controller.getHospitalByNameAndRegion(getHospital().getName(), 1L))
                .thenReturn(ResponseEntity.ok(getHospital()));

        when(controller.getAllHospitalNamesInRegion(1L))
                .thenReturn(ResponseEntity.ok(Arrays.asList(getHospital())));

        when(controller.getHospitalByNameAndRegion("FakeName", 99L))
                .thenReturn(ResponseEntity.status(HttpStatus.NOT_FOUND).body(String.format
                        ("Not found the hospital with name [%s]", "FakeName")));

    }

    public Hospital getHospital() {
        Hospital hospital = new Hospital();
        hospital.setId(1L);
        hospital.setName("Hospital");
        return hospital;
    }

    @Test
    public void testGetHospitalByNameAndRegion() throws Exception {

        String expected = "{\"id\":1,\"name\":\"Hospital\",\"region\":null}";
        mockMvc
                .perform(get("/hospital/")
                        .param("hospitalName", "Hospital")
                        .param("regionId", "1"))
                .andExpect(status().isOk())
                .andExpect(content().string(expected))
                .andDo(print());
    }

    @Test
    public void testGetFakeHospitalByNameAndRegion() throws Exception {

        String expected = String.format
                ("Not found the hospital with name [%s]", "FakeName");
        mockMvc
                .perform(get("/hospital/")
                        .param("hospitalName", "FakeName")
                        .param("regionId", "99"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expected))
                .andDo(print());
    }

    @Test
    public void testGetAllHospitalNames() throws Exception {

        String expected = "[{\"id\":1,\"name\":\"Hospital\",\"region\":null}]";
        mockMvc
                .perform(get("/hospital/all/1"))
                .andExpect(status().isOk())
                .andExpect(content().string(expected))
                .andDo(print());

    }
}