package com.epam.bbqueue.controller;

import com.epam.bbqueue.entity.TimeSlot;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("deprecation")
@RunWith(SpringRunner.class)
@WebMvcTest(secure = false, value = {VisitResultController.class, GlobalExceptionHandler.class})
public class VisitResultControllerTest{

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VisitResultController resultController;

    @Before
    public void initQueueController(){
        when(resultController.getAllHistoryOfVisitsForUser())
                .thenReturn(ResponseEntity.ok(Arrays.asList(createTimeSlot())));
        when(resultController.getAllActiveQueuePlace())
                .thenReturn(ResponseEntity.ok(Arrays.asList(createTimeSlot())));
    }

    public TimeSlot createTimeSlot(){
        TimeSlot timeSlot = new TimeSlot();
        timeSlot.setId(1L);
        timeSlot.setTimeSlotNumber(1);
        return timeSlot;
    }

    @Test
    public void testGetAllHistoryOfVisitsForUser() throws Exception {
        String expected = "[{\"id\":1,\"visitDate\":null,\"timeSlotNumber\":1," +
                "\"customer\":null,\"service\":null,\"visitResult\":null}]";
        mockMvc
                .perform(get("/user/visits-history/"))
                .andExpect(status().isOk())
                .andExpect(content().string(expected))
                .andDo(print());
    }

    @Test
    public void testGetAllActiveQueuePlace() throws Exception {
        String expected = "[{\"id\":1,\"visitDate\":null,\"timeSlotNumber\":1," +
                "\"customer\":null,\"service\":null,\"visitResult\":null}]";
        mockMvc
                .perform(get("/user/visits-active/"))
                .andExpect(status().isOk())
                .andExpect(content().string(expected))
                .andDo(print());
    }
}
