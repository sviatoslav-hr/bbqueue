package com.epam.bbqueue.controller;

import com.epam.bbqueue.entity.DoctorSpeciality;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("deprecation")
@RunWith(SpringRunner.class)
@WebMvcTest(secure = false, value = {SearchSpecialityFormController.class})
public class SearchSpecialityFormControllerTest {

    @MockBean
    private SearchSpecialityFormController controller;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void init() {

        when(controller.getSpecialityByName(getDoctorSpeciality().getName()))
                .thenReturn(ResponseEntity.ok(getDoctorSpeciality()));

        when(controller.getSpecialityByName("FakeName"))
                .thenReturn(ResponseEntity.status(HttpStatus.NOT_FOUND).body(String.format
                        ("Not found the speciality with name [%s]", "FakeName")));

    }

    public DoctorSpeciality getDoctorSpeciality() {
        DoctorSpeciality doctorSpeciality = new DoctorSpeciality();
        doctorSpeciality.setId(1L);
        doctorSpeciality.setName("Speciality");
        return doctorSpeciality;
    }

    @Test
    public void testGetSpecialityByName() throws Exception {

        String expected = "{\"id\":1,\"name\":\"Speciality\"}";

        mockMvc
                .perform(get("/speciality/Speciality"))
                .andExpect(status().isOk())
                .andExpect(content().string(expected))
                .andDo(print());

    }

    @Test
    public void testGetFakeSpecialityByName() throws Exception {

        String expected = String.format
                ("Not found the speciality with name [%s]", "FakeName");

        mockMvc
                .perform(get("/speciality/FakeName"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expected))
                .andDo(print());

    }
}