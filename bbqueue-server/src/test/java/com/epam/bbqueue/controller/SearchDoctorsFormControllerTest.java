package com.epam.bbqueue.controller;

import com.epam.bbqueue.dto.DoctorSearchFormDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("deprecation")
@RunWith(SpringRunner.class)
@WebMvcTest(secure = false, value = {SearchDoctorsFormController.class})
public class SearchDoctorsFormControllerTest {

    @MockBean
    private SearchDoctorsFormController controller;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void init() {
        when(controller.getDoctorsByHospital(1L))
                .thenReturn(ResponseEntity.ok(Arrays.asList(getDoctor())));

        when(controller.getDoctorsBySpecialityAndHospitalAndDoctorName(1L,
                1L, "Bolit"))
                .thenReturn(ResponseEntity.ok(Arrays.asList(getDoctor())));

        when(controller.getDoctorsByHospitalAndDoctorName(1L, "Bolit"))
                .thenReturn(ResponseEntity.ok(Arrays.asList(getDoctor())));

        when(controller.getDoctorsBySpecialityAndHospital("NameSpeciality", 1L))
                .thenReturn(ResponseEntity.ok(Arrays.asList(getDoctor())));

        when(controller.getDoctorsByHospital(33L))
                .thenReturn(ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body("In this hospital not found doctors!"));

        when(controller.getDoctorsBySpecialityAndHospitalAndDoctorName(33L,
                33L, "Bolt"))
                .thenReturn(ResponseEntity.status(HttpStatus.NOT_FOUND).body(String.format
                        ("Not found doctors with speciality id [%s] and hospital id [%s] and name [%s]!",
                                33, 33, "Bolt")));

        when(controller.getDoctorsByHospitalAndDoctorName(33L, "Bolt"))
                .thenReturn(ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body("In this hospital not found doctors!"));

        when(controller.getDoctorsBySpecialityAndHospital("FakeSpeciality", 33L))
                .thenReturn(ResponseEntity.status(HttpStatus.NOT_FOUND).body(String.format
                        ("Not found doctors with speciality name [%s] and hospital id [%s]!",
                                "FakeSpeciality", 33)));

    }

    public DoctorSearchFormDto getDoctor() {
        return DoctorSearchFormDto.builder()
                .id(1L)
                .cabinetNumber(1)
                .firstName("Ai")
                .lastName("Bolit")
                .build();
    }

    @Test
    public void testGetDoctorsByHospital() throws Exception {

        String expected = "[{\"id\":1,\"cabinetNumber\":1,\"firstName\":\"Ai\"," +
                "\"lastName\":\"Bolit\",\"speciality\":null,\"hospital\":null}]";
        mockMvc
                .perform(get("/doctors/in-hospital/1"))
                .andExpect(status().isOk())
                .andExpect(content().string(expected))
                .andDo(print());

    }

    @Test
    public void testGetFakeDoctorsByHospital() throws Exception {

        String expected = "In this hospital not found doctors!";
        mockMvc
                .perform(get("/doctors/in-hospital/33"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expected))
                .andDo(print());

    }

    @Test
    public void testGetDoctorsBySpecialityAndHospital() throws Exception {

        String expected = "[{\"id\":1,\"cabinetNumber\":1,\"firstName\":\"Ai\"," +
                "\"lastName\":\"Bolit\",\"speciality\":null,\"hospital\":null}]";
        mockMvc
                .perform(get("/doctors/with-speciality")
                        .param("specialityName", "NameSpeciality")
                        .param("hospitalId", "1"))
                .andExpect(status().isOk())
                .andExpect(content().string(expected))
                .andDo(print());

    }

    @Test
    public void testGetFakeDoctorsBySpecialityAndHospital() throws Exception {

        String expected = String.format
                ("Not found doctors with speciality name [%s] and hospital id [%s]!",
                        "FakeSpeciality", 33);
        mockMvc
                .perform(get("/doctors/with-speciality")
                        .param("specialityName", "FakeSpeciality")
                        .param("hospitalId", "33"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expected))
                .andDo(print());

    }

    @Test
    public void testGetDoctorsBySpecialityAndHospitalAndDoctorName() throws Exception {

        String expected = "[{\"id\":1,\"cabinetNumber\":1,\"firstName\":\"Ai\"," +
                "\"lastName\":\"Bolit\",\"speciality\":null,\"hospital\":null}]";
        mockMvc
                .perform(get("/doctors/with-speciality-and-name")
                        .param("specialityId", "1")
                        .param("hospitalId", "1")
                        .param("lastName", "Bolit"))
                .andExpect(status().isOk())
                .andExpect(content().string(expected))
                .andDo(print());

    }

    @Test
    public void testGetFakeDoctorsBySpecialityAndHospitalAndDoctorName() throws Exception {

        String expected = String.format
                ("Not found doctors with speciality id [%s] and hospital id [%s] and name [%s]!",
                        33, 33, "Bolt");
        mockMvc
                .perform(get("/doctors/with-speciality-and-name")
                        .param("specialityId", "33")
                        .param("hospitalId", "33")
                        .param("lastName", "Bolt"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expected))
                .andDo(print());

    }

    @Test
    public void testGetDoctorsByHospitalAndDoctorName() throws Exception {

        String expected = "[{\"id\":1,\"cabinetNumber\":1,\"firstName\":\"Ai\"," +
                "\"lastName\":\"Bolit\",\"speciality\":null,\"hospital\":null}]";
        mockMvc
                .perform(get("/doctors/by-name")
                        .param("hospitalId", "1")
                        .param("lastName", "Bolit"))
                .andExpect(status().isOk())
                .andExpect(content().string(expected))
                .andDo(print());

    }

    @Test
    public void testGetFakeDoctorsByHospitalAndDoctorName() throws Exception {

        String expected = "In this hospital not found doctors!";
        mockMvc
                .perform(get("/doctors/by-name")
                        .param("hospitalId", "33")
                        .param("lastName", "Bolt"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expected))
                .andDo(print());

    }
}