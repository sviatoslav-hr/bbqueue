package com.epam.bbqueue.controller;

import com.epam.bbqueue.entity.Region;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("deprecation")
@RunWith(SpringRunner.class)
@WebMvcTest(secure = false, value = {SearchRegionFormController.class})
public class SearchRegionFormControllerTest {

    @MockBean
    private SearchRegionFormController controller;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void init() {

        when(controller.getRegionByName(getRegion().getName())).thenReturn(ResponseEntity.ok(getRegion()));

        when(controller.getAllRegionNames()).thenReturn(ResponseEntity.ok(Arrays.asList(getRegion())));

        when(controller.getRegionByName("FakeName"))
                .thenReturn(ResponseEntity.status(HttpStatus.NOT_FOUND).body(String.format
                        ("Not found the region with name [%s]", "FakeName")));

    }

    public Region getRegion() {
        Region region = new Region();
        region.setId(1L);
        region.setName("Region");
        return region;
    }

    @Test
    public void testGetRegionByName() throws Exception {

        String expected = "{\"id\":1,\"name\":\"Region\",\"country\":null}";
        mockMvc
                .perform(get("/region/Region"))
                .andExpect(status().isOk())
                .andExpect(content().string(expected))
                .andDo(print());

    }

    @Test
    public void testGetFakeRegionByName() throws Exception {

        String expected = String.format
                ("Not found the region with name [%s]", "FakeName");
        mockMvc
                .perform(get("/region/FakeName"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(expected))
                .andDo(print());

    }

    @Test
    public void testGetAllRegionNames() throws Exception {

        String expected = "[{\"id\":1,\"name\":\"Region\",\"country\":null}]";
        mockMvc
                .perform(get("/region/all"))
                .andExpect(status().isOk())
                .andExpect(content().string(expected))
                .andDo(print());

    }
}