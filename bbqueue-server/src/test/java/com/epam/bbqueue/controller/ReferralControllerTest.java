package com.epam.bbqueue.controller;

import com.epam.bbqueue.dto.ReferralDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Date;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("deprecation")
@RunWith(SpringRunner.class)
@WebMvcTest(secure = false, value = {ReferralController.class, GlobalExceptionHandler.class})
public class ReferralControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReferralController referralController;

    @Before
    public void initQueueController(){
        when(referralController.getReferralForUser())
        .thenReturn(ResponseEntity.ok(Arrays.asList(createReferral())));
    }

    public ReferralDto createReferral(){
        return ReferralDto.builder()
                .id(1L)
                .description("fever")
                .doctorFromName("Bob")
                .doctorToName("Alan")
                .customerName("Igor")
                .expirationDate(new Date(119,8,24,18,20,12))
                .build();
    }

    @Test
    public void testGetReferralForUser() throws Exception {
        String expected = "[{\"id\":1,\"description\":\"fever\",\"doctorFromName\":\"Bob\"," +
                "\"doctorToName\":\"Alan\",\"customerName\":\"Igor\",\"expirationDate\":1569338412000}]";
        mockMvc
                .perform(get("/user/referral/" ))
                .andExpect(status().isOk())
                .andExpect(content().string(expected))
                .andDo(print());
    }
}
